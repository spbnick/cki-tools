"""YAML CLI tools for miscellaneous operations not supported by shyaml."""
import argparse
import sys
import typing

import yaml


def set_value(data: typing.Any, key_path: str, value: typing.Any) -> typing.Any:
    """Implement the set-value command."""
    target = data
    keys = key_path.split('.')
    key: typing.Union[str, int]
    for index, key in enumerate(keys):
        if isinstance(target, list):
            key = int(key)
        if index < len(keys) - 1:
            target = target[key]
        else:
            target[key] = yaml.safe_load(value)


def main(args: typing.Sequence[str]) -> None:
    """Run CLI YAML tool."""
    parser = argparse.ArgumentParser(description='Miscellaneous YAML operations')

    parser.add_argument('action', choices=('set-value',), help='Action to perform')
    parser.add_argument('key', help='Target for the action')
    parser.add_argument('value', help='YAML-formatted value')
    parsed_args = parser.parse_args(args)

    data = yaml.safe_load(sys.stdin)

    if parsed_args.action == 'set-value':
        set_value(data, parsed_args.key, parsed_args.value)
    else:
        raise Exception(f'Unknown action {parsed_args.action}')

    yaml.safe_dump(data, stream=sys.stdout)


if __name__ == '__main__':
    main(sys.argv[1:])
