"""Base metric classes."""
import datetime
import threading

from cki_lib.logger import get_logger
import crontab

LOGGER = get_logger(__name__)


class Metric:
    """Metric base class."""

    schedule = '*/5 * * * * * *'

    def __init__(self):
        """Initialize."""
        self._last_run_datetime = None
        self._running = threading.Event()
        LOGGER.info('Initialized %s', self.__class__.__name__)

    def update(self, *, last_run_datetime=None):
        """
        Update the metrics.

        This method is called every time the metric is updated and should
        perform all the necessary steps to update the prometheus metrics
        this class exposes.
        """
        raise NotImplementedError

    def _update(self, last_run_datetime):
        """Update and clear the _running flag."""
        LOGGER.info('Updating %s', type(self).__name__)
        try:
            self.update(last_run_datetime=last_run_datetime)
        finally:
            self._running.clear()
        LOGGER.debug('Finished updating %s', type(self).__name__)

    def _next(self):
        tab = crontab.CronTab(self.schedule)
        next_run = tab.next(now=self._last_run_datetime, return_datetime=True)
        return next_run.replace(tzinfo=datetime.timezone.utc)

    def check_and_update(self):
        """Check the time since the last update and, if necessary, call update()."""
        now = datetime.datetime.utcnow().replace(tzinfo=datetime.timezone.utc)

        if not self._last_run_datetime or self._next() <= now:
            if self._running.is_set():
                LOGGER.error("Task '%s' scheduled to run but it's still running.",
                             type(self).__name__)
                return

            last_run_datetime, self._last_run_datetime = self._last_run_datetime, now
            self._running.set()
            threading.Thread(target=self._update, args=[last_run_datetime], daemon=True).start()
