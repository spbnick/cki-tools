"""Export information about GitLab."""
import os
from urllib import parse

from cki_lib.gitlab import get_instance
import prometheus_client
import yaml

from .. import base

GITLAB_CONFIG = yaml.safe_load(os.environ.get('GITLAB_CONFIG', ''))
KNOWN_PLANS = ['free', 'premium', 'ultimate']


class GitLabMetrics(base.Metric):
    """Calculate GitLab metrics."""

    schedule = '0 * * * *'  # once per hour

    metric_namespace_plan = prometheus_client.Enum(
        'cki_gitlab_namespace_plan',
        'Paid plan for a namespace',
        ['instance', 'namespace'],
        states=KNOWN_PLANS + ['unknown'],
    )

    def update(self, **_):
        """Update the metrics."""
        for namespace_url in GITLAB_CONFIG.get('namespaces', []):
            url_parts = parse.urlsplit(namespace_url)

            instance_url = parse.urlunparse(url_parts[:2] + ('',) * 4)
            gl_instance = get_instance(instance_url)
            gl_namespace = gl_instance.namespaces.get(url_parts.path[1:])

            plan = gl_namespace.plan if gl_namespace.plan in KNOWN_PLANS else 'unknown'
            # pylint: disable=no-member
            self.metric_namespace_plan.labels(gl_instance.url, gl_namespace.full_path).state(plan)
