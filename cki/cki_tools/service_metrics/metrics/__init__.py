"""Actual metrics."""

from .awscosts import AwsCostMetrics
from .beaker import BeakerMetrics
from .gitlab import GitLabMetrics
from .kubernetes import KubernetesMetrics
from .s3buckets import S3BucketMetrics
from .sentry import SentryMetrics
from .teiidbeaker import TEIIDBeakerMetrics
from .volume import VolumeMetrics

ALL_METRICS = [
    AwsCostMetrics,
    BeakerMetrics,
    GitLabMetrics,
    KubernetesMetrics,
    S3BucketMetrics,
    SentryMetrics,
    TEIIDBeakerMetrics,
    VolumeMetrics,
]
