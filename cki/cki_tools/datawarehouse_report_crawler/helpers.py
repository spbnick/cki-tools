"""Helpers."""
from email.utils import parseaddr
import json

from cki_lib.session import get_session

SESSION = get_session(__name__)


def get_checkout_report(datawarehouse_url, message):
    """Get a report from a checkout."""
    msg_id = parseaddr(message.msg.get('Message-ID'))[1]

    response = SESSION.get(
        f'{datawarehouse_url}/api/1/kcidb/checkouts/{message.checkout_iid}/reports/{msg_id}')
    assert response.status_code == 200
    return response.json()


def submit_report(datawarehouse_url, datawarehouse_token, message):
    """Send the sent emails to the Datawarehouse."""
    payload = {
        'content': message.msg.as_string(),
    }

    headers = {
        'Authorization': f'Token {datawarehouse_token}',
        'Content-Type': 'application/json'
    }

    response = SESSION.post(
        f'{datawarehouse_url}/api/1/kcidb/checkouts/{message.checkout_iid}/reports',
        data=json.dumps(payload), headers=headers
    )

    assert response.status_code == 201, response.reason


def get_checkout_iid(datawarehouse_url, pipeline_id):
    """Get checkout iid from Datawarehouse."""
    response = SESSION.get(f'{datawarehouse_url}/api/1/pipeline/{pipeline_id}')
    return response.json()['checkout_iid']
