"""Classes."""
import datetime
import mailbox
import os

from cki_lib.mailarchive_crawler import MailArchiveCrawler
from cki_lib.pipeline_message import PipelineMsgBase


class MMessageBase(PipelineMsgBase):
    """Extend PipelineMsgBase by loading email list archives."""

    @staticmethod
    def _interesting_files():
        """Return a list of the filenames we want to parse."""
        today = datetime.datetime.now()
        months = [
            today.strftime('%Y-%B.txt')
        ]

        if today.day < 7:
            # Just in case, include previous month too.
            months.append(
                (today - datetime.timedelta(days=30)).strftime('%Y-%B.txt')
            )

        return months

    @classmethod
    def listarchives2msgs(cls, src_path):
        """Load from archives list."""
        files = [os.path.join(src_path, filename)
                 for filename in cls._interesting_files()]

        results = []
        for file in files:
            messages = mailbox.mbox(file)
            for message in messages:
                try:
                    results.append(cls(message))
                except Exception:  # pylint: disable=broad-except
                    print("Failed to create message: ", message)

        return results


class LatestMailArchiveCrawler(MailArchiveCrawler):
    """LatestMailArchiveCrawler."""

    def get_archives_months(self):
        """Override get_archives_months to only return the latest 2 months."""
        results = super().get_archives_months()
        return results[:2]
