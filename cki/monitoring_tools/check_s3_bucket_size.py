"""Check the usage of an S3 bucket against quota and threshold."""
import argparse
import sys

from cki.cki_tools import _utils


def get_bucket_size(bucket_spec, ignore_prefix=None):
    """Return bucket size (in bytes) and objects count."""
    bucket = _utils.S3Bucket(bucket_spec).bucket
    prefix = '' if ignore_prefix else bucket_spec.prefix
    size = 0
    count = 0
    for bucket_object in bucket.objects.filter(Prefix=prefix):
        size += bucket_object.size
        count += 1
    return size, count


def main(args):
    """Parse the command line options and summarize an S3 bucket."""
    parser = argparse.ArgumentParser(
        description='Check the usage of an S3 bucket against a quota.')
    parser.add_argument('bucket_spec', action=_utils.StoreBucketSpec,
                        help='name of a bucket spec environment variable')
    parser.add_argument('--quota', metavar='SIZE', type=float,
                        help='bucket quota in GB')
    parser.add_argument('--threshold', metavar='SIZE', type=float,
                        help='threshold in GB for notification via exit code')
    parser.add_argument('--ignore-prefix', action='store_true',
                        help='report the usage for the whole bucket')
    args = parser.parse_args(args)

    size, count = get_bucket_size(args.bucket_spec, args.ignore_prefix) / 1e9

    if args.quota:
        used = size / args.quota * 100
        print(f'{used:.1f}% [{size:.1f} GB, {count} objects]')
    else:
        print(f'[{size:.1f} GB, {count} objects]')

    return 1 if args.threshold and size >= args.threshold else 0


if __name__ == '__main__':
    sys.exit(main(sys.argv[1:]))
