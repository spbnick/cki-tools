"""Wait until beaker finishes processing recipes."""
import argparse
import os
import pathlib
import time

from cki_lib.logger import get_logger
from cki_lib.session import get_session
from rcdefinition.rc_data import parse_config_data

LOGGER = get_logger(__name__)
SESSION = get_session(__name__)

BEAKER_URL = os.environ['BEAKER_URL']


def get_recipes(recipeset_id):
    """Get a list of recipes from a recipeset."""
    url = f'{BEAKER_URL}/recipesets/{recipeset_id}'
    response = SESSION.get(url)
    recipes = response.json()['machine_recipes']
    return recipes


def wait_for_recipesets(recipesets_ids):
    """
    Wait until all recipesets are finished in Beaker.

    It's necessary to make sure no task has result="New" before downloading
    the results.
    """
    while True:
        running = False
        for recipeset_id in recipesets_ids:
            recipes = get_recipes(recipeset_id)
            for recipe in recipes:
                for task in recipe['tasks']:
                    if task['result'] == 'New':
                        LOGGER.info('Recipe %s - Task %s: Result is "New"',
                                    recipe['id'], task['id'])
                        running = True

        if not running:
            LOGGER.info("All done!")
            return

        LOGGER.info("Waiting 60 seconds before checking again.")
        time.sleep(60)


def main(argv=None):
    """Wait until all recipes are completed."""
    parser = argparse.ArgumentParser(
        description='Wait until Beaker finishes processing recipes results.'
    )
    parser.add_argument('--rc', default='rc', help="Path to rc file.")
    args = parser.parse_args(argv)

    rc_data = parse_config_data(
        pathlib.Path(args.rc).read_text(encoding='utf8')
    )

    recipesets_ids = [
        recipeset.split(':')[1]
        for recipeset in rc_data['state'].get('recipesets', '').split()
    ]

    wait_for_recipesets(recipesets_ids)


if __name__ == '__main__':
    main()
