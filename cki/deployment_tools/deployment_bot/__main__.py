"""Deployment bot CLI interface."""
import argparse
import os
import pathlib
import sys

from cki_lib import config_tree
import yaml

from . import deployment


def main(args):
    """CLI Interface."""
    parser = argparse.ArgumentParser(
        description='Listen to deployment events and trigger deployment pipelines')
    parser.add_argument('--config-path',
                        default=os.environ.get('DEPLOYMENT_BOT_CONFIG_PATH', 'config.yml'),
                        help='Path to the config file')
    parser.add_argument('--message',
                        help='Webhook message body for a manual deployment')
    parsed_args = parser.parse_args(args)

    config = config_tree.process_config_tree(yaml.safe_load(
        os.environ.get('DEPLOYMENT_BOT_CONFIG') or
        pathlib.Path(parsed_args.config_path).read_text(encoding='utf8')))

    if parsed_args.message:
        deployment.Deployment(config).callback(body=yaml.safe_load(parsed_args.message))
    else:
        deployment.Deployment(config).listen()


if __name__ == '__main__':
    main(sys.argv[1:])
