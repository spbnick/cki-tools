"""Autoscaler CLI."""
import argparse
import json
import os
import pathlib
import time

from cki_lib import config_tree
from cki_lib import metrics
from cki_lib import misc
import sentry_sdk
import yaml

from cki.deployment_tools.autoscaler import Application

CONFIG_PATH = os.environ.get('AUTOSCALER_CONFIG_PATH', 'config.yaml')
REFRESH_PERIOD = misc.get_env_int('REFRESH_PERIOD', 30)


def parse_args():
    """Parse arguments."""
    parser = argparse.ArgumentParser(
        description='Monitor queue and autoscale service',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter
    )
    parser.add_argument('--config-path', default=CONFIG_PATH,
                        help='Path to the config file where to load services from')
    parser.add_argument('--service', help='Check a single service and exit')
    parser.add_argument('--refresh-period', type=int, default=REFRESH_PERIOD,
                        help='Time (in seconds) between checks')
    return parser.parse_args()


def main():
    """CLI Interface."""
    misc.sentry_init(sentry_sdk)
    metrics.prometheus_init()

    args = parse_args()

    if os.environ.get('AUTOSCALER_CONFIG'):
        config = json.loads(os.environ['AUTOSCALER_CONFIG'])
    else:
        config = yaml.load(
            pathlib.Path(args.config_path).read_text(encoding='utf8'),
            Loader=yaml.FullLoader
        )

    config = config_tree.process_config_tree(config)

    if args.service:
        Application(args.service, config[args.service]).check()
        return

    services = [
        Application(service_name, service_config)
        for service_name, service_config in config.items()
    ]

    while True:
        for service in services:
            service.check()

        time.sleep(args.refresh_period)


if __name__ == '__main__':
    main()
