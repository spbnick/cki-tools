"""Adjust kcidb dumpfiles."""
import glob
import json
import os
import pathlib

from cki_lib.misc import get_logger
from cki_lib.misc import utc_now_iso
from kcidb_io.schema import validate

LOGGER = get_logger(__name__)
CURRENT_TIME = utc_now_iso()


def append_upt_test_results(kcidb_objects):
    """Append UPT kcidb test dumpfiles into KCIDB_DUMPFILE_NAME / kcidb_all.json."""
    any_change = False

    kcidb_objects.setdefault('tests', [])
    for fname in glob.glob(f'{os.environ["UPT_RESULTS"]}*/results_*/kcidb_*.json'):
        kcidb_objects['tests'] += json.loads(
            pathlib.Path(fname).read_text(encoding='utf8'))['tests']
        pathlib.Path(fname).unlink()
        any_change = True

    return any_change


def dump(objects, path):
    """Validate and dump objects."""
    validate(objects)
    path.write_text(json.dumps(objects, indent=4), encoding='utf8')


def adjust_file(path, func):
    """Adjust file at path of obj_class using func."""
    LOGGER.debug('Trying to adjust file %s', path.name)
    kcidb_objects = json.loads(path.read_text())
    any_change = func(kcidb_objects)
    if any_change:
        dump(kcidb_objects, path)


def adjust_pipeline_dumpfiles(path2file):
    """Add pipeline finished_at and duration to pipeline dumpfiles."""
    path = pathlib.Path(path2file)
    if path.is_file():
        adjust_file(path, append_upt_test_results)


def main():
    """Do all."""
    adjust_pipeline_dumpfiles(os.environ['KCIDB_DUMPFILE_NAME'])


if __name__ == '__main__':
    main()
