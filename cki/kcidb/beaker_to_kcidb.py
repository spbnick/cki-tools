"""Convert Beaker XML to KCIDB test plan."""
import argparse
import os
import pathlib
import sys
import xml.etree.ElementTree as ET

from cki_lib.kcidb.file import KCIDBFile


def get_param(task, key):
    """Return value for given key param."""
    for param in task.iter('param'):
        if param.get('name') == key:
            return param.get('value')
    return None


def xml_to_kcidb(beaker_xml_path, build_id):
    """Extract list of KCDIB test from beaker xml."""
    content = pathlib.Path(beaker_xml_path).read_bytes()
    root = ET.fromstring(content)
    tests = []

    for recipeset in root.iter('recipe'):
        for task in recipeset.iter('task'):
            cki_id = get_param(task, 'CKI_ID')
            if not cki_id:
                # Not a CKI task
                continue

            tests.append(
                {
                    'id': f'{build_id}_upt_{cki_id}',
                    'origin': 'redhat',
                    'build_id': build_id,
                    'comment': get_param(task, 'CKI_NAME'),
                    'path': get_param(task, 'CKI_UNIVERSAL_ID'),
                }
            )

    return tests


def parse_args(argv):
    """Parse command line arguments."""
    parser = argparse.ArgumentParser(
        description='Convert Beaker XML to KCIDB.'
    )

    parser.add_argument(
        '--beaker-xml', default=os.environ.get('BEAKER_TEMPLATE_PATH'), type=str,
        help='A path to the Beaker XML file.',
    )

    parser.add_argument(
        '--kcidb-file', default=os.environ.get('KCIDB_DUMPFILE_NAME'), type=str,
        help='A path to the Beaker XML file.',
    )

    parser.add_argument(
        '--build-id', default=os.environ.get('KCIDB_BUILD_ID'), type=str,
        help='KCIDB Build ID.',
    )

    return parser.parse_args(argv)


def main(argv):
    """Generate test plan from Beaker XML."""
    args = parse_args(argv)
    kcidb_file = KCIDBFile(args.kcidb_file)

    for test in xml_to_kcidb(args.beaker_xml, args.build_id):
        kcidb_file.set_test(test['id'], test)

    kcidb_file.save()


if __name__ == '__main__':
    main(sys.argv[1:])
