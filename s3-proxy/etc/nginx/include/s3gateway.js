/*
 *  Copyright 2020 F5 Networks
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

const mod_hmac = require('crypto');
const querystring = require('querystring');

const awsAccessKeyId = process.env['AWS_ACCESS_KEY_ID'];
const awsSecretAccessKey = process.env['AWS_SECRET_ACCESS_KEY'];
const awsDefaultRegion = process.env['AWS_DEFAULT_REGION'] || 'us-east-1';
const awsEndpointHost = process.env['AWS_ENDPOINT_HOST'] || 's3.' + awsDefaultRegion + '.amazonaws.com';
const awsEndpointScheme = process.env['AWS_ENDPOINT_SCHEME'] || 'https';

const now = new Date();
const eightDigitDate =
    _padWithLeadingZeros(now.getUTCFullYear(), 4) +
    _padWithLeadingZeros(now.getUTCMonth() + 1, 2) +
    _padWithLeadingZeros(now.getUTCDate(), 2);
const amzDatetime = eightDigitDate + 'T' +
    _padWithLeadingZeros(now.getUTCHours(), 2) +
    _padWithLeadingZeros(now.getUTCMinutes(), 2) +
    _padWithLeadingZeros(now.getUTCSeconds(), 2) + 'Z';

const emptyPayloadHash = 'e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855';
const signedHeaders = 'host;x-amz-content-sha256;x-amz-date';

/** Returns the proxied S3 URI */
function s3Uri(r) {
    return awsEndpointScheme + '://' + awsEndpointHost +
        _buildCanonicalUri(r) + '?' + _buildCanonicalQueryString(r);
}

/** Returns the Host header */
function s3HeaderHost(r) {
    return awsEndpointHost;
}

/** Returns the Authorization header */
function s3HeaderAuthorization(r) {
    return 'AWS4-HMAC-SHA256 Credential=' +
        awsAccessKeyId + '/' +
        eightDigitDate + '/' +
        awsDefaultRegion + '/s3/aws4_request,SignedHeaders=' +
        signedHeaders + ',Signature=' + _buildSignatureV4(r);
}

/** Returns the 'x-amz-date' header */
function s3HeaderXAmzDate(r) {
    return amzDatetime;
}

/** Returns the 'x-amz-content-sha256' header */
function s3HeaderXAmzContentSha256(r) {
    return emptyPayloadHash;
}

/** Construct a canonical uri */
function _buildCanonicalUri(r) {
    return r.uri.split('/').map(x => encodeURIComponent(x)).join('/');
}

/** Construct a canonical query string */
function _buildCanonicalQueryString(r) {
    return Object.keys(r.args).sort().map(
        key => key + '=' + querystring.escape(querystring.unescape(r.args[key]))
    ).join('&');
}

/**
 * Creates a signature for use authenticating against an AWS compatible API.
 *
 * @see {@link https://docs.aws.amazon.com/general/latest/gr/signature-version-4.html | AWS V4 Signing Process}
 * @param r {Request} HTTP request object
 * @param eightDigitDate {string} date in the form of 'YYYYMMDD'
 * @returns {string} hex encoded hash of signature HMAC value
 * @private
 */
function _buildSignatureV4(r) {
    const canonicalRequest = _buildCanonicalRequest(r.method,
        _buildCanonicalUri(r),
        _buildCanonicalQueryString(r));

    r.log('AWS v4 Auth Canonical Request: [' + canonicalRequest + ']');

    const canonicalRequestHash = mod_hmac.createHash('sha256')
        .update(canonicalRequest)
        .digest('hex');

    r.log('AWS v4 Auth Canonical Request Hash: [' + canonicalRequestHash + ']');

    const stringToSign = _buildStringToSign(canonicalRequestHash)

    r.log('AWS v4 Auth Signing String: [' + stringToSign + ']');

    const kSigningHash = _buildSigningKeyHash();

    r.log('AWS v4 Signing Key Hash: [' + kSigningHash.toString('hex') + ']');

    const signature = mod_hmac.createHmac('sha256', kSigningHash)
        .update(stringToSign).digest('hex');

    r.log('AWS v4 Authorization Header: [' + signature + ']');

    return signature;
}

/**
 * Creates a string to sign by concatenating together multiple parameters required
 * by the signatures algorithm.
 *
 * @see {@link https://docs.aws.amazon.com/general/latest/gr/sigv4-create-string-to-sign.html | String to Sign}
 * @param canonicalRequestHash {string} hex encoded hash of canonical request string
 * @returns {string} a concatenated string of the passed parameters formatted for signatures
 * @private
 */
function _buildStringToSign(canonicalRequestHash) {
    return 'AWS4-HMAC-SHA256\n' +
        amzDatetime + '\n' +
        eightDigitDate + '/' +
        awsDefaultRegion + '/s3/aws4_request\n' +
        canonicalRequestHash;
}

/**
 * Creates a canonical request that will later be signed
 *
 * @see {@link https://docs.aws.amazon.com/general/latest/gr/sigv4-create-canonical-request.html | Creating a Canonical Request}
 * @param method {string} HTTP method
 * @param uri {string} URI associated with request
 * @param queryParams {string} query parameters associated with request
 * @param host {string} HTTP Host header value
 * @returns {string} string with concatenated request parameters
 * @private
 */
function _buildCanonicalRequest(method, uri, queryParams) {
    return method + '\n' +
        uri + '\n' +
        queryParams + '\n' +
        'host:' + awsEndpointHost + '\n' +
        'x-amz-content-sha256:' + emptyPayloadHash + '\n' +
        'x-amz-date:' + amzDatetime + '\n\n' +
        signedHeaders + '\n' +
        emptyPayloadHash;
}

/**
 * Creates a signing key HMAC. This value is used to sign the request made to
 * the API.
 *
 * @returns {ArrayBuffer} signing HMAC
 * @private
 */
function _buildSigningKeyHash() {
    const kDate = mod_hmac.createHmac('sha256', 'AWS4'.concat(awsSecretAccessKey))
        .update(eightDigitDate).digest();
    const kRegion = mod_hmac.createHmac('sha256', kDate)
        .update(awsDefaultRegion).digest();
    const kService = mod_hmac.createHmac('sha256', kRegion)
        .update('s3').digest();
    const kSigning = mod_hmac.createHmac('sha256', kService)
        .update('aws4_request').digest();

    return kSigning;
}

/**
 * Pads the supplied number with leading zeros.
 *
 * @param num {number|string} number to pad
 * @param size number of leading zeros to pad
 * @returns {string} a string with leading zeros
 * @private
 */
function _padWithLeadingZeros(num, size) {
    return String(num).padStart(size, '0');
}

export default {
    s3Uri,
    s3HeaderHost,
    s3HeaderAuthorization,
    s3HeaderXAmzDate,
    s3HeaderXAmzContentSha256,
};
