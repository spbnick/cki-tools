# 🤖  Datawarehouse Triager 🤖

`cki.triager`

Detect and report pipeline failures.

This project aims to automatically triage common pipeline failures.
Using simple checks and regexes, datawarehouse-triager is able to
report failures on the Datawarehouse.

## Configuration

| Environment variable             | Description                                               |
|----------------------------------|-----------------------------------------------------------|
| `IS_PRODUCTION`                  | Define if the environment is staging or production.       |
| `DATAWAREHOUSE_URL`              | URL of Datawarehouse.                                     |
| `DATAWAREHOUSE_TOKEN_TRIAGER`    | Token for Datawarehouse authentication.                   |
| `DATAWAREHOUSE_EXCHANGE_TRIAGER` | Exchange where Datawarehouse publishes objects to triage. |
| `DATAWAREHOUSE_QUEUE_TRIAGER`    | Queue to use for binding to Datawarehouse exchange.       |

### IS_PRODUCTION

On staging developments (`IS_PRODUCTION=False`), the issues found are not
sent to Datawarehouse.
