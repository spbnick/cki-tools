# Copyright (c) 2021 Red Hat, Inc. All rights reserved. This copyrighted
# material is made available to anyone wishing to use, modify, copy, or
# redistribute it subject to the terms and conditions of the GNU General Public
# License v.2 or later.
#
# This program is distributed in the hope that it will be useful, but WITHOUT
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
# FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with
# this program; if not, write to the Free Software Foundation, Inc., 51
# Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
"""Gitlab api interaction tests."""
from io import StringIO
import tempfile
import unittest
from unittest.mock import patch

from rcdefinition.rc_data import SKTData

from cki.cki_tools import rc_edit


class TestRCEDit(unittest.TestCase):
    """Test TriggerVariables class."""

    def setUp(self) -> None:
        self.data = SKTData()
        self.edit = rc_edit.RCEdit(self.data)

    def test_rc_set(self):
        """Ensure rc_set works."""
        # Bash passes everything as str, do we do 'True' -> True.
        edit, data = self.edit, self.data
        edit.rc_set('build', 'valid', 'True')
        self.assertEqual(data.build.valid, True)

        edit.rc_set('build', 'duration', '100')
        self.assertEqual(data.build.duration, 100)

        edit.rc_set('build', 'duration', '100')
        self.assertEqual(data.build.duration, 100)

        edit.rc_state_set('patch_data', '["a","b","c"]')
        self.assertEqual(data.state.patch_data, ['a', 'b', 'c'])

        edit.rc_set('state', 'does_not_exist', 123)
        result = edit.rc_state_get('does_not_exist')
        self.assertEqual(data.state.does_not_exist, 123, result)

        edit.rc_state_del('does_not_exist')
        self.assertEqual(data.state.does_not_exist, None)

        edit.rc_set('checkout', 'comment', 'somedescr')
        self.assertEqual(data.checkout.comment, 'somedescr')

    def test_main(self):
        with tempfile.NamedTemporaryFile() as rcfile:
            with patch('sys.argv', ['rc_edit.py', rcfile.name, 'rc_set', 'build', 'valid', 'True']):
                rc_edit.main()
            with patch('sys.stdout', new=StringIO()) as mock_stdout:
                with patch('sys.argv', ['rc_edit.py', rcfile.name, 'rc_get', 'build', 'valid']):
                    rc_edit.main()
                self.assertEqual(mock_stdout.getvalue(), 'True\n')
