"""Test edit.py"""
import contextlib
import io
import json
from pathlib import Path
import tempfile
import unittest
from unittest import mock

from cki_lib.kcidb.file import KCIDBFile
from freezegun import freeze_time

from cki.kcidb.edit import main


class TestKCIDBEdit(unittest.TestCase):
    """Test edit.py."""

    # Copied from cki_lib
    checkout1 = {
        'id': 'redhat:checkout1',
        'origin': 'redhat'
    }
    checkout2 = {
        'id': 'redhat:checkout2',
        'origin': 'redhat'
    }

    # Copied from cki_lib
    build1 = {
        'id': 'redhat:build1',
        'checkout_id': 'redhat:checkout1',
        'origin': 'redhat'
    }
    build2 = {
        'id': 'redhat:build2',
        'checkout_id': 'redhat:checkout2',
        'origin': 'redhat'
    }
    test1 = {
        'id': 'redhat:test1',
        'build_id': 'redhat:build1',
        'origin': 'redhat'
    }
    test2 = {
        'id': 'redhat:test2',
        'build_id': 'redhat:build1',
        'origin': 'redhat'
    }

    # Copied from cki_lib
    valid_content = {
        'version': {'major': 4},
        'tests': [],
    }

    # Copied from cki_lib
    @classmethod
    @contextlib.contextmanager
    def dump_kcidb_json(cls, kcidb_data):
        tmpdir = tempfile.TemporaryDirectory()
        path = Path(tmpdir.name, 'kcidb_data.json')
        path.write_text(json.dumps(kcidb_data))
        try:
            yield path
        finally:
            tmpdir.cleanup()

    # Copied from cki_lib
    @classmethod
    @contextlib.contextmanager
    def create_kcidb_file(cls, kcidb_data):
        with cls.dump_kcidb_json(kcidb_data) as path:
            kcidb_file = KCIDBFile(str(path))
            try:
                yield (path, kcidb_file)
            finally:
                pass

    def test_set(self):
        """Test set."""
        kcidb_data = {**self.valid_content, 'builds': [self.build1]}
        for command, key, value in (
            ('set', 'id', 'redhat:build2'),
            ('set-bool', 'valid', True),
            ('set-int', 'duration', 0),
        ):
            with self.subTest(command=command):
                with self.create_kcidb_file(kcidb_data) as (kcidb_file_path, kcidb_file):
                    main([str(kcidb_file_path), 'build', 'redhat:build1', command, key, str(value)])
                    kcidb_file2 = KCIDBFile(str(kcidb_file_path))
                    self.assertEqual(value, kcidb_file2.build[key])

    def test_set_nested(self):
        """Test set of nested keys."""
        kcidb_data = {
            **self.valid_content,
            'checkouts': [
                {'id': 'redhat:1234', 'origin': 'redhat'}
            ]
        }
        with self.create_kcidb_file(kcidb_data) as (kcidb_file_path, kcidb_file):
            main([str(kcidb_file_path), 'checkout', 'redhat:1234',
                  'set-bool', 'misc/debug', 'true'])
            kcidb_file2 = KCIDBFile(str(kcidb_file_path))
            self.assertEqual(True, kcidb_file2.checkout['misc']['debug'])

    def test_set_raises_type(self):
        """Test set-bool and set-int raise if the value passed is not the correct type."""
        kcidb_data = {**self.valid_content, 'builds': [self.build1]}
        for command, key, value in (
            ('set-bool', 'valid', 'Tru'),
            ('set-int', 'duration', '0..'),
        ):
            with self.subTest(command=command):
                with self.create_kcidb_file(kcidb_data) as (kcidb_file_path, kcidb_file):
                    with self.assertRaises(SystemExit):
                        main([str(kcidb_file_path), 'build', 'redhat:build1',
                              command, 'valid', value])

    def test_set_raises_partial(self):
        """Test set raises if a valid attribute is set but the file is still not valid."""
        for attr in ('build', 'checkout'):
            with self.subTest(attr=attr):
                with self.create_kcidb_file(self.valid_content) as (kcidb_file_path, kcidb_file):
                    id = f'redhat:{attr}2'
                    with self.assertRaises(Exception):
                        main([str(kcidb_file_path), attr, id, 'set', 'id', id])

    @freeze_time('2021-01-01T00:00:00.0+00:00')
    def test_set_time_now(self):
        """Test set time with 'now' value."""
        kcidb_data = {
            **self.valid_content,
            'checkouts': [
                {'id': 'redhat:1234', 'origin': 'redhat'}
            ]
        }
        with self.create_kcidb_file(kcidb_data) as (kcidb_file_path, kcidb_file):
            main([str(kcidb_file_path), 'checkout', 'redhat:1234',
                  'set-time', 'start_time', 'now'])
            kcidb_file2 = KCIDBFile(str(kcidb_file_path))
            self.assertEqual('2021-01-01T00:00:00+00:00', kcidb_file2.checkout['start_time'])

    def test_set_time_iso(self):
        """Test set time with correctly formatted value."""
        kcidb_data = {
            **self.valid_content,
            'checkouts': [
                {'id': 'redhat:1234', 'origin': 'redhat'}
            ]
        }
        with self.create_kcidb_file(kcidb_data) as (kcidb_file_path, kcidb_file):
            main([str(kcidb_file_path), 'checkout', 'redhat:1234',
                  'set-time', 'start_time', '2021-01-01T00:01:02+00:00'])

            kcidb_file2 = KCIDBFile(str(kcidb_file_path))
            self.assertEqual('2021-01-01T00:01:02+00:00', kcidb_file2.checkout['start_time'])

    def test_set_time_formatting(self):
        """Test set time correctly formats the value."""
        kcidb_data = {
            **self.valid_content,
            'checkouts': [
                {'id': 'redhat:1234', 'origin': 'redhat'}
            ]
        }
        with self.create_kcidb_file(kcidb_data) as (kcidb_file_path, kcidb_file):
            main([str(kcidb_file_path), 'checkout', 'redhat:1234',
                  'set-time', 'start_time', '2021-01-01 10:21 UTC'])
            kcidb_file2 = KCIDBFile(str(kcidb_file_path))
            self.assertEqual('2021-01-01T10:21:00+00:00', kcidb_file2.checkout['start_time'])

    def test_get(self):
        """Test get."""
        for attr, kcidb_data in (
            ('build', {**self.valid_content, 'builds': [self.build1]}),
            ('checkout', {**self.valid_content, 'checkouts': [self.checkout1]}),
            ('test', {**self.valid_content, 'tests': [self.test1]}),
        ):
            with self.subTest(attr=attr):
                with self.create_kcidb_file(kcidb_data) as (kcidb_file_path, kcidb_file):
                    with mock.patch('sys.stdout', new_callable=io.StringIO) as mock_stdout:
                        main([str(kcidb_file_path), attr, f'redhat:{attr}1', 'get', 'id'])
                        self.assertEqual(mock_stdout.getvalue(),
                                         f'{kcidb_data[f"{attr}s"][0]["id"]}\n')

    def test_get_nested(self):
        """Test get nested keys."""
        kcidb_data = {
            **self.valid_content,
            'checkouts': [
                {'id': 'redhat:1234', 'origin': 'redhat', 'misc': {'debug': True}}
            ]
        }
        with self.create_kcidb_file(kcidb_data) as (kcidb_file_path, kcidb_file):
            with mock.patch('sys.stdout', new_callable=io.StringIO) as mock_stdout:
                main([str(kcidb_file_path), 'checkout', 'redhat:1234', 'get', 'misc/debug'])
                self.assertEqual(mock_stdout.getvalue(), 'True\n')

    def test_create(self):
        for attr, obj_id, args in (
            ('checkout', self.checkout1['id'], []),
            ('build', self.build1['id'], [self.build1['checkout_id']]),
            ('test', self.test1['id'], [self.test1['build_id']]),
        ):
            with tempfile.TemporaryDirectory() as tmpdir:
                kcidb_file_path = Path(tmpdir, 'kcidb_data.json')
                main([str(kcidb_file_path), attr, obj_id, 'create'] + args)
                self.assertEqual(
                    getattr(KCIDBFile(str(kcidb_file_path)), f'get_{attr}')(obj_id),
                    getattr(self, f'{attr}1'),
                )

    def test_create_raises(self):
        for attr, obj_id, args in (
            ('checkout', 'INVALID_ID', []),
            ('build', 'INVALID_ID', ['INVALID_CHECKOUT_ID']),
        ):
            with tempfile.TemporaryDirectory() as tmpdir:
                kcidb_file_path = Path(tmpdir, 'kcidb_data.json')
                with self.assertRaises(Exception):
                    main([str(kcidb_file_path), attr, obj_id, 'create'] + args)

    def test_get_multiple(self):
        """Test get call when there are multiple objects in the file."""
        kcidb_data = {
            **self.valid_content,
            'checkouts': [self.checkout1, self.checkout2],
            'builds': [self.build1, self.build2],
        }

        cases = [
            ('checkout', 'redhat:checkout1'),
            ('checkout', 'redhat:checkout2'),
            ('build', 'redhat:build1'),
            ('build', 'redhat:build2'),
        ]

        for attr, obj_id in cases:
            with self.subTest(attr=attr, obj_id=obj_id):
                with self.create_kcidb_file(kcidb_data) as (kcidb_file_path, kcidb_file):
                    with mock.patch('sys.stdout', new_callable=io.StringIO) as mock_stdout:
                        main([str(kcidb_file_path), attr, obj_id, 'get', 'id'])
                        self.assertEqual(mock_stdout.getvalue(), f'{obj_id}\n')

    def test_set_multiple(self):
        """Test set call when there are multiple objects in the file."""
        kcidb_data = {
            **self.valid_content,
            'checkouts': [self.checkout1, self.checkout2],
            'builds': [self.build1, self.build2],
            'tests': [self.test1, self.test2],
        }

        for attr in ['checkout', 'build', 'test']:
            with self.subTest(attr=attr):
                with self.create_kcidb_file(kcidb_data) as (kcidb_file_path, kcidb_file):
                    main([str(kcidb_file_path), attr, f'redhat:{attr}1', 'set', 'misc/foo', 'bar'])
                    getter = getattr(KCIDBFile(str(kcidb_file_path)), f'get_{attr}')
                    self.assertEqual(getter(f'redhat:{attr}1')['misc']['foo'], 'bar')

                    # Make sure it didn't touch redhat:{attr}2
                    self.assertEqual(
                        getter(f'redhat:{attr}2'), getattr(self, f'{attr}2')
                    )

    def test_create_multiple(self):
        """Test create call multiple times."""
        with self.create_kcidb_file(self.valid_content) as (kcidb_file_path, kcidb_file):
            main([str(kcidb_file_path), 'checkout', 'redhat:1', 'create'])
            main([str(kcidb_file_path), 'checkout', 'redhat:2', 'create'])
            main([str(kcidb_file_path), 'build', 'redhat:2', 'create', 'redhat:1'])
            main([str(kcidb_file_path), 'build', 'redhat:3', 'create', 'redhat:1'])
            main([str(kcidb_file_path), 'test', 'redhat:4', 'create', 'redhat:2'])

            self.assertEqual(
                KCIDBFile(str(kcidb_file_path)).data,
                {
                    'version': {'major': 4},
                    'checkouts': [
                        {'id': 'redhat:1', 'origin': 'redhat'},
                        {'id': 'redhat:2', 'origin': 'redhat'},
                    ],
                    'builds': [
                        {'id': 'redhat:2', 'checkout_id': 'redhat:1', 'origin': 'redhat'},
                        {'id': 'redhat:3', 'checkout_id': 'redhat:1', 'origin': 'redhat'},
                    ],
                    'tests': [
                        {'id': 'redhat:4', 'build_id': 'redhat:2', 'origin': 'redhat'},
                    ],
                }
            )

    def test_add_append_dict(self):
        """Test append_dict."""
        kcidb_data = {**self.valid_content, 'builds': [self.build1]}
        name = 'coverage result'
        url = 'http://localhost?name1=value1&name2=value2'
        with self.create_kcidb_file(kcidb_data) as (kcidb_file_path, kcidb_file):
            main([str(kcidb_file_path), 'build', 'redhat:build1', 'append-dict',
                  'output_files', f'name={name}', f'url={url}'])
            kcidb_file2 = KCIDBFile(str(kcidb_file_path))
            self.assertEqual(True, isinstance(kcidb_file2.build['output_files'], list))
            self.assertEqual(1, len(kcidb_file2.build['output_files']))
            self.assertEqual({'name': name, 'url': url},
                             kcidb_file2.build['output_files'][0])
