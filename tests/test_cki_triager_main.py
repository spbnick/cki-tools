"""Test main.py."""
import unittest
from unittest import mock

from tests.utils import tear_down_registry

tear_down_registry()

# pylint: disable=wrong-import-position
from cki.triager import main  # noqa: E402


class TestCallback(unittest.TestCase):
    """Test Callback."""

    @mock.patch('cki.triager.main.triager.Triager.check')
    def test_not_new(self, check):
        """Test object status is not new."""
        msg = {
            'object_type': 'checkout',
            'object': {'id': 1},
            'status': 'something',
        }
        ack_fn = mock.Mock()
        main.callback(msg, ack_fn, dry_run=False)
        self.assertFalse(check.called)
        self.assertTrue(ack_fn.called)

    @mock.patch('cki.triager.main.triager.Triager.check')
    def test_dry_run(self, check):
        """Test object status is new but dry_run."""
        msg = {
            'object_type': 'checkout',
            'object': {'id': 1},
            'status': 'new',
        }
        ack_fn = mock.Mock()
        main.callback(msg, ack_fn, dry_run=True)
        self.assertTrue(check.called)
        self.assertFalse(ack_fn.called)

        check.assert_called_with('checkout', {'id': 1})

    @mock.patch('cki.triager.main.triager.Triager.check')
    def test_ok(self, check):
        """Test object status is new."""
        msg = {
            'object_type': 'checkout',
            'object': {'id': 1},
            'status': 'new',
        }
        ack_fn = mock.Mock()
        main.callback(msg, ack_fn, dry_run=False)
        self.assertTrue(check.called)
        self.assertTrue(ack_fn.called)

        check.assert_called_with('checkout', {'id': 1})

    @mock.patch('cki.triager.main.triager.Triager.check')
    def test_retriage(self, check):
        """Test object status is needs_triage."""
        msg = {
            'object_type': 'checkout',
            'object': {'id': 1},
            'status': 'needs_triage',
        }
        ack_fn = mock.Mock()
        main.callback(msg, ack_fn, dry_run=False)
        self.assertTrue(check.called)
        self.assertTrue(ack_fn.called)

        check.assert_called_with('checkout', {'id': 1})


class TestTriageSingle(unittest.TestCase):
    """Test triage_single."""

    @mock.patch('cki.triager.main.triager.Triager.check')
    def test_call(self, check):
        """Test call."""
        main.triage_single('checkout', 'redhat:123', dry_run=True)
        self.assertTrue(check.called)
        check.assert_called_with('checkout', 'redhat:123')


class TestArgumentParsing(unittest.TestCase):
    """Test the argument parsing."""

    @mock.patch('cki.triager.main.IS_PRODUCTION', True)
    def test_production(self):
        """Test the argument handling in production mode."""
        self.assertEqual(main.parse_args([]).dry_run, False)
        self.assertEqual(main.parse_args(['--dry-run']).dry_run, True)
        self.assertEqual(main.parse_args(['--no-dry-run']).dry_run, False)

    @mock.patch('cki.triager.main.IS_PRODUCTION', False)
    def test_non_production(self):
        """Test the argument handling in non-production mode."""
        self.assertEqual(main.parse_args([]).dry_run, True)
        self.assertEqual(main.parse_args(['--dry-run']).dry_run, True)
        self.assertEqual(main.parse_args(['--no-dry-run']).dry_run, False)
