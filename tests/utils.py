"""Utility functions for testcases."""
from prometheus_client.registry import REGISTRY


def tear_down_registry():
    # pylint: disable=protected-access
    """Unregister collectors in prometheus registry."""
    collectors = list(REGISTRY._collector_to_names.keys())
    for collector in collectors:
        REGISTRY.unregister(collector)
