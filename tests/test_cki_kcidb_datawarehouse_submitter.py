"""KCIDB convert tests."""
import json
import pathlib
import unittest
from unittest import mock

from kcidb_wrap import v4

from cki.kcidb.datawarehouse_submitter import process_jobs
from tests.fakes import FakePipeline


def download_and_unzip_artifacts(job, tmpdirname):
    obj_with_misc = {
        'misc': {
            'job': {
                'name': job.name,
                'stage': job.stage
            }
        }
    }
    content = {
        'version': {
            'major': 4,
            'minor': 0
        },
        'checkouts': [obj_with_misc],
        'builds': [obj_with_misc],
        'tests': [obj_with_misc]
    }

    pathlib.Path(f'{tmpdirname}/kcidb_all.json').write_text(
        json.dumps(content),
        encoding='utf8')


class TestKCIDBConvert(unittest.TestCase):
    """Test cki.kcidb.datawarehouse_submitter."""

    @mock.patch('cki.kcidb.datawarehouse_submitter.download_and_unzip_artifacts',
                side_effect=download_and_unzip_artifacts)
    def test_process_jobs(self, unzip):
        # setup, send-report, prepare, and prepare-builder stages don't have kcidb data, but
        # including them here checks that get_upt_objects filters them out as expected
        job_args = [
            (1275903957, 'test s390x', 'test', 'success'),
            (1275903954, 'test ppc64le', 'test', 'success'),
            (1275903950, 'test aarch64', 'test', 'success'),
            (1275903947, 'test x86_64', 'test', 'success'),
            (1275903945, 'setup s390x', 'setup', 'success'),
            (1275903943, 'setup ppc64le', 'setup', 'success'),
            (1275903942, 'setup aarch64', 'setup', 'success'),
            (1275903941, 'setup x86_64', 'setup', 'success'),
            (1275903940, 'publish s390x', 'publish', 'success'),
            (1275903937, 'publish ppc64le', 'publish', 'success'),
            (1275903931, 'publish aarch64', 'publish', 'success'),
            (1275903928, 'publish x86_64', 'publish', 'success'),
            (1275903924, 'build s390x', 'build', 'success'),
            (1275903920, 'build ppc64le', 'build', 'success'),
            (1275903917, 'build aarch64', 'build', 'success'),
            (1275903913, 'build x86_64', 'build', 'success'),
            (1275903909, 'merge', 'merge', 'success'),
            (1275903903, 'send-report', 'review', 'manual'),
            (1275903898, 'prepare-builder', 'prepare', 'success'),
            (1275903893, 'prepare', 'prepare', 'success')
        ]
        pipeline = FakePipeline('branch', 'token', {'test_mode': 'upt', 'name': 'stable'}, 'status')
        for args in job_args:
            pipeline.add_job(*args)
        kcidb_json = process_jobs(pipeline, pipeline.jobs)

        # only latest stage is used
        output_jobs = [
            ('test s390x', 'test'),
            ('test ppc64le', 'test'),
            ('test aarch64', 'test'),
            ('test x86_64', 'test'),
        ]
        list_of_objs = []
        for name, stage in output_jobs:
            list_of_objs.append(
                {
                    'misc': {
                        'job': {
                            'name': name,
                            'stage': stage
                        },
                        'pipeline': {
                            'variables': {
                                'test_mode': 'upt',
                                'name': 'stable'
                            }
                        }
                    }
                }
            )
        expected = v4.craft_kcidb_data(list_of_objs, list_of_objs, list_of_objs)
        self.assertEqual(kcidb_json, expected)
