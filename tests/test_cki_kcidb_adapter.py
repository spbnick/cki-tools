"""KCIDB adapter tests."""

import contextlib
import json
import os
import pathlib
import tempfile
import unittest
from unittest import mock

from rcdefinition.rc_data import SKTData
import responses

from cki.kcidb import adapter

ENV_DEFAULT = {
    'CI_COMMIT_REF_NAME': 'upstream-stable',
    'CI_JOB_ID': '1234',
    'CI_JOB_YMD': '2021/01/01',
    'CI_PIPELINE_ID': '5678',
    'CI_PROJECT_ID': '1',
    'CI_PROJECT_PATH': 'project/path',
    'CI_SERVER_URL': 'https://gitlab.com',

    # Pipeline trigger variables
    'cki_pipeline_branch': 'branch',
    'cki_pipeline_type': 'type',
    'name': 'upstream-stable',
}

ENV_CHECKOUT = {
    **ENV_DEFAULT,
    'git_url': 'https://git.url',
    'commit_hash': '65a5ecf4d079182ebccf078f3cb103515a598b47',
    'branch': 'upstream-stable',
    'message_id': 'message@id.com',
}

ENV_BUILD = {
    **ENV_DEFAULT,
    'CI_JOB_STAGE': 'build',
    'architecture': 'aarch64',
    'config_target': 'target',
}

RC_FILE_CONTENT = '''
[state]
mergelog = whatever-path-to-merge-log
patch_subjects = patch_1 patch_2

config_file = whatever-path-to-config
buildlog = whatever-path-to-build-log

tarball_file = path/to/tarball
selftests_file = path/to/selftests_file
selftests_buildlog = path/to/selftests_buildlog

repo_path = something
kernel_package_url = https://kpackage.url

debug_kernel = True
'''


@contextlib.contextmanager
def mock_ci_project_dir():
    """Replace ${CI_PROJECT_DIR} with temporary dir."""
    tmp_dir = tempfile.TemporaryDirectory()
    with mock.patch.dict(os.environ, {'CI_PROJECT_DIR': tmp_dir.name}):
        try:
            yield
        finally:
            tmp_dir.cleanup()


@contextlib.contextmanager
def tmp_kcidb_file(kcidb_file_content):
    """Dump kcidb_file_content into temporary file and return filename."""
    tmp_file = tempfile.NamedTemporaryFile()

    pathlib.Path(tmp_file.name).write_text(
        json.dumps(kcidb_file_content)
    )

    yield tmp_file.name


@contextlib.contextmanager
def tmp_rc_file(rc_file_content):
    """Dump rc_file_content into ${CI_PROJECT_DIR}/rc."""
    pathlib.Path(os.environ['CI_PROJECT_DIR'], 'rc').write_text(
        rc_file_content
    )

    yield


KCIDB_DEFAULT = {
    'version': {'major': 4, 'minor': 0}
}


@mock.patch.dict(os.environ, ENV_DEFAULT)
class TestKCIDBAdapter(unittest.TestCase):
    """Test cki.kcidb.adapter.KCIDBAdapter."""

    @mock_ci_project_dir()
    def test_init(self):
        """Test default init."""
        with tmp_kcidb_file(KCIDB_DEFAULT) as kcidb_file:
            with tmp_rc_file(RC_FILE_CONTENT):
                kcidb_adapter = adapter.KCIDBAdapter('obj_id', kcidb_file)

        self.assertEqual(KCIDB_DEFAULT, kcidb_adapter.kcidb_file.data)
        self.assertEqual('obj_id', kcidb_adapter.obj_id)
        self.assertEqual(True, kcidb_adapter.upload)

        self.assertEqual(SKTData.deserialize(RC_FILE_CONTENT).to_mapping(),
                         kcidb_adapter.rc_data.to_mapping())
        self.assertEqual('private', kcidb_adapter.visibility)
        self.assertEqual('2021/01/01/5678', kcidb_adapter.artifacts_path_prefix)

    @mock_ci_project_dir()
    def test_init_visibility(self):
        """Test visibility value."""
        test_cases = [
            ('upstream', 'public'),
            ('internal', 'private'),
            ('foo', 'private'),
            ('', 'private')
        ]

        with tmp_kcidb_file(KCIDB_DEFAULT) as kcidb_file:
            for kernel_type, expected_visibility in test_cases:
                with mock.patch.dict(os.environ, {'kernel_type': kernel_type}):
                    self.assertEqual(
                        expected_visibility,
                        adapter.KCIDBAdapter('id', kcidb_file).visibility
                    )

    @mock_ci_project_dir()
    @mock.patch('cki.kcidb.adapter.utils.upload_file')
    def test_upload_file_false(self, mock_upload_file):
        """Test upload_file with upload=False."""
        with tmp_kcidb_file(KCIDB_DEFAULT) as kcidb_file:
            kcidb_adapter = adapter.KCIDBAdapter('id', kcidb_file, upload=False)

        kcidb_adapter.upload_file('/path', 'file_name')
        self.assertFalse(mock_upload_file.called)

    @mock_ci_project_dir()
    @mock.patch('cki.kcidb.adapter.utils.upload_file')
    def test_upload_file(self, mock_upload_file):
        """Test upload_file with upload=True."""
        with tmp_kcidb_file(KCIDB_DEFAULT) as kcidb_file:
            kcidb_adapter = adapter.KCIDBAdapter('id', kcidb_file, upload=True)

        kcidb_adapter.upload_file('/path', 'file_name', file_content='content', source_path='path')
        self.assertTrue(mock_upload_file.called)
        mock_upload_file.assert_called_with(
            kcidb_adapter.visibility,
            '/path', 'file_name',
            file_content='content', source_path='path'
        )

    @mock_ci_project_dir()
    def test_generate(self):
        """Test generate returns {} when NotImplemented."""
        with tmp_kcidb_file(KCIDB_DEFAULT) as kcidb_file:
            kcidb_adapter = adapter.KCIDBAdapter('id', kcidb_file, upload=True)

        self.assertRaises(NotImplementedError, kcidb_adapter.generate)

    @mock_ci_project_dir()
    def test_update(self):
        """Test dump output."""
        initial_objects = {
            'version': {'major': 4, 'minor': 0},
            'checkouts': [
                {'id': 'redhat:1', 'origin': 'redhat'},
                {'id': 'redhat:2', 'origin': 'redhat'},
            ],
            'builds': [
                {'id': 'redhat:1', 'origin': 'redhat', 'checkout_id': 'redhat:1'},
                {'id': 'redhat:2', 'origin': 'redhat', 'checkout_id': 'redhat:1'},
            ],
            'tests': [{'id': 'redhat:1', 'origin': 'redhat', 'build_id': 'redhat:1'}],
        }
        dump_objects = {
            'checkouts': [
                {'id': 'redhat:1', 'origin': 'redhat', 'valid': False},
            ],
            'builds': [
                {'id': 'redhat:1', 'origin': 'redhat', 'checkout_id': 'redhat:1', 'valid': False},
            ]
        }
        expected_output = {
            'version': {'major': 4, 'minor': 0},
            'checkouts': [
                {'id': 'redhat:1', 'origin': 'redhat', 'valid': False},
                {'id': 'redhat:2', 'origin': 'redhat'},
            ],
            'builds': [
                {'id': 'redhat:1', 'origin': 'redhat', 'checkout_id': 'redhat:1', 'valid': False},
                {'id': 'redhat:2', 'origin': 'redhat', 'checkout_id': 'redhat:1'},
            ],
            'tests': [{'id': 'redhat:1', 'origin': 'redhat', 'build_id': 'redhat:1'}],
        }

        with tmp_kcidb_file(initial_objects) as kcidb_file:
            kcidb_adapter = adapter.KCIDBAdapter('id', kcidb_file)

        kcidb_adapter.generate = mock.Mock()
        kcidb_adapter.dump = mock.Mock()
        kcidb_adapter.dump.return_value = dump_objects

        result = kcidb_adapter.update()
        self.assertEqual(expected_output, result.data)

    def test_gitlab_data(self):
        """Test gitlab_data."""
        data = adapter.KCIDBAdapter.gitlab_data()

        self.assertEqual(
            {
                'job': {
                    'id': 1234
                },
                'pipeline': {
                    'id': 5678,
                    'project': {
                        'id': 1,
                        'instance_url': 'https://gitlab.com',
                        'path_with_namespace': 'project/path'
                    },
                    'ref': 'upstream-stable',
                    'variables': {
                        'cki_pipeline_type': 'type',
                        'cki_pipeline_branch': 'branch',
                        'name': 'upstream-stable'
                    }
                }
            }, data)

    def test_remove_empty_values(self):
        """Test remove_empty_values."""
        data = {
            'a': 'a',
            'b': None,
            'c': [],
            'd': {},
        }

        self.assertEqual(
            {'a': 'a'},
            adapter.KCIDBAdapter.remove_empty_values(data)
        )


@mock.patch.dict(os.environ, ENV_CHECKOUT)
class TestCheckout(unittest.TestCase):
    """Test cki.kcidb.adapter.Checkout."""

    @mock.patch.dict(os.environ, ENV_CHECKOUT)
    @mock_ci_project_dir()
    def setUp(self):
        self.kcidb_content = {
            **KCIDB_DEFAULT,
            'checkouts': [
                {'id': 'redhat:1234', 'origin': 'redhat'}
            ]
        }

        with tmp_kcidb_file(self.kcidb_content) as kcidb_file:
            self.checkout = adapter.Checkout('redhat:1234', kcidb_file)

    def test_generate(self):
        """Test generate return values."""
        self.checkout.generate()
        self.assertEqual(
            {
                'checkouts': [
                    {
                        'id': 'redhat:1234',
                        'origin': 'redhat',
                        'git_commit_hash': '65a5ecf4d079182ebccf078f3cb103515a598b47',
                        'git_repository_branch': 'upstream-stable',
                        'git_repository_url': 'https://git.url',
                        'patchset_hash': '',
                        'message_id': 'message@id.com',
                        'tree_name': 'upstream-stable',
                        'misc': adapter.KCIDBAdapter.gitlab_data(),
                    }
                ]
            }, self.checkout.dump())

    @mock.patch.dict(os.environ, {'coverage': 'True'})
    def test_generate_with_coverage(self):
        """Test generate return values."""
        self.checkout.generate()
        self.assertEqual(
            {
                'checkouts': [
                    {
                        'id': 'redhat:1234',
                        'origin': 'redhat',
                        'git_commit_hash': '65a5ecf4d079182ebccf078f3cb103515a598b47',
                        'git_repository_branch': 'upstream-stable',
                        'git_repository_url': 'https://git.url',
                        'patchset_hash': '',
                        'message_id': 'message@id.com',
                        'tree_name': 'upstream-stable-coverage',
                        'misc': adapter.KCIDBAdapter.gitlab_data(),
                    }
                ]
            }, self.checkout.dump())

    @mock_ci_project_dir()
    @mock.patch.dict(os.environ, {'patch_urls': 'https://s/p_1 https://s/p_2/'})
    @responses.activate
    def test_generate_with_patches(self):
        """Test generate return values."""
        responses.add(responses.GET, 'https://s/p_1', body=b'p_1')
        responses.add(responses.GET, 'https://s/p_2/', body=b'p_2')

        with mock.patch.dict(
                KCIDB_DEFAULT,
                {'checkouts': [{'id': 'redhat:1234', 'origin': 'redhat'}]}):
            with tmp_kcidb_file(KCIDB_DEFAULT) as kcidb_file:
                checkout = adapter.Checkout('redhat:1234', kcidb_file)

        checkout.generate()

        self.assertEqual(
            {
                'checkouts': [
                    {
                        'id': 'redhat:1234',
                        'origin': 'redhat',
                        'git_commit_hash': '65a5ecf4d079182ebccf078f3cb103515a598b47',
                        'git_repository_branch': 'upstream-stable',
                        'git_repository_url': 'https://git.url',
                        'message_id': 'message@id.com',
                        'tree_name': 'upstream-stable',
                        'misc': adapter.KCIDBAdapter.gitlab_data(),
                        'patchset_files': [
                            {'name': 'p_1', 'url': 'https://s/p_1'},
                            {'name': 'p_2', 'url': 'https://s/p_2/'}
                        ],
                        'patchset_hash': (
                            '8d2e6de35099fc8e1c9f98f6c107c004'
                            'f414846d30f6315a23ccbe0d9a664328'
                        ),
                    }
                ]
            }, checkout.dump())

    @mock.patch('cki.kcidb.adapter.utils.patch_list_hash')
    def test_patchset_hash(self, mock_patch_list_hash):
        """Test patchset_hash return values."""
        mock_patch_list_hash.return_value = 'patch hash'

        with mock.patch.dict(os.environ, {'patch_urls': 'https://s/p_1 https://s/p_2/'}):
            self.assertEqual('patch hash', self.checkout.patchset_hash)

    def test_artifacts_path(self):
        """Test artifacts_path value."""
        self.assertEqual('2021/01/01/5678/redhat:1234', self.checkout.artifacts_path)

    @mock_ci_project_dir()
    def test_log_url(self):
        """Test log_url value."""
        rc_content = f"""
            [state]
            mergelog = {os.environ["CI_PROJECT_DIR"]}/path
        """
        pathlib.Path(os.environ['CI_PROJECT_DIR'], 'path').write_text('foo')

        with tmp_kcidb_file(self.kcidb_content) as kcidb_file:
            with tmp_rc_file(rc_content):
                checkout = adapter.Checkout('redhat:1234', kcidb_file)

        checkout.upload_file = mock.Mock()
        checkout.upload_file.return_value = 'http://some/url'

        self.assertEqual('http://some/url', checkout.log_url)
        checkout.upload_file.assert_called_with(
            '2021/01/01/5678/redhat:1234', 'merge.log',
            source_path=f'{os.environ["CI_PROJECT_DIR"]}/path'
        )

    @mock_ci_project_dir()
    def test_log_url_file_missing(self):
        """Test log_url value."""
        rc_content = """
            [state]
            mergelog = path/to/log
        """

        with tmp_kcidb_file(self.kcidb_content) as kcidb_file:
            with tmp_rc_file(rc_content):
                checkout = adapter.Checkout('redhat:1234', kcidb_file)

        self.assertEqual(None, checkout.log_url)

    @mock_ci_project_dir()
    def test_log_url_empty(self):
        """Test log_url value when there's no merge log."""
        with tmp_kcidb_file(self.kcidb_content) as kcidb_file:
            with tmp_rc_file(''):
                checkout = adapter.Checkout('redhat:1234', kcidb_file)

        self.assertEqual(None, checkout.log_url)


@mock.patch.dict(os.environ, ENV_BUILD)
class TestBuild(unittest.TestCase):
    """Test cki.kcidb.adapter.Build."""

    @mock.patch.dict(os.environ, ENV_BUILD)
    @mock_ci_project_dir()
    def setUp(self):
        """Set up tests."""
        self.kcidb_content = {
            **KCIDB_DEFAULT,
            'checkouts': [{'id': 'redhat:1234', 'origin': 'redhat'}],
            'builds': [
                {'id': 'redhat:5678', 'checkout_id': 'redhat:1234', 'origin': 'redhat',
                 'architecture': 'aarch64'}
            ]
        }

        with tmp_kcidb_file(self.kcidb_content) as kcidb_file:
            self.build = adapter.Build('redhat:5678', kcidb_file)

    def test_generate(self):
        """Test generate return values."""
        self.build.generate()
        self.assertEqual(
            {
                'builds': [
                    {
                        'id': 'redhat:5678',
                        'checkout_id': 'redhat:1234',
                        'origin': 'redhat',
                        'architecture': 'aarch64',
                        'comment': 'CKI build of upstream-stable',
                        'config_name': 'target',
                        'misc': adapter.KCIDBAdapter.gitlab_data(),
                    }
                ],
                'tests': [],
            }, self.build.dump())

    def test_generate_with_selftests(self):
        """Test generate return values when selftests are generated."""
        self.build.selftests = [
            {'id': 'redhat:5678_0', 'build_id': 'redhat:5678', 'origin': 'redhat'}
        ]

        self.build.generate()

        self.assertEqual(
            {
                'builds': [
                    {
                        'id': 'redhat:5678',
                        'checkout_id': 'redhat:1234',
                        'origin': 'redhat',
                        'architecture': 'aarch64',
                        'comment': 'CKI build of upstream-stable',
                        'config_name': 'target',
                        'misc': adapter.KCIDBAdapter.gitlab_data(),
                    }
                ],
                'tests': self.build.selftests,
            }, self.build.dump())

    @mock_ci_project_dir()
    def test_selftest(self):
        """Test selftests generation."""
        with mock.patch.dict(os.environ, {'SELFTESTS_BUILD_RESULTS_PATH':
                                          f'{os.environ["CI_PROJECT_DIR"]}/selftest'}):
            pathlib.Path(os.environ['SELFTESTS_BUILD_RESULTS_PATH']).write_text(
                'test_1: 0\ntest_2: 1'
            )

            selftests = self.build.selftests

        self.assertEqual(
            [
                {
                    'id': 'redhat:5678_selftest_0',
                    'build_id': 'redhat:5678',
                    'origin': 'redhat',
                    'comment': 'kselftest build - test_1',
                    'path': 'kselftest-build.test_1',
                    'status': 'PASS',
                    'waived': False,
                    'misc': adapter.KCIDBAdapter.gitlab_data(),
                },
                {
                    'id': 'redhat:5678_selftest_1',
                    'build_id': 'redhat:5678',
                    'origin': 'redhat',
                    'comment': 'kselftest build - test_2',
                    'path': 'kselftest-build.test_2',
                    'status': 'FAIL',
                    'waived': False,
                    'misc': adapter.KCIDBAdapter.gitlab_data(),
                },
            ], selftests)

    @mock_ci_project_dir()
    def test_selftest_different_stage(self):
        """Test selftests are not generated on other stages than build."""
        with mock.patch.dict(os.environ, {'SELFTESTS_BUILD_RESULTS_PATH':
                                          f'{os.environ["CI_PROJECT_DIR"]}/selftest',
                                          'CI_JOB_STAGE': 'publish'}):
            pathlib.Path(os.environ['SELFTESTS_BUILD_RESULTS_PATH']).write_text(
                'test_1: 0\ntest_2: 1'
            )

            selftests = self.build.selftests

        self.assertEqual([], selftests)

    @mock_ci_project_dir()
    def test_selftest_empty(self):
        """Test selftests generation when file does not exist."""
        with mock.patch.dict(os.environ, {'SELFTESTS_BUILD_RESULTS_PATH':
                                          f'{os.environ["CI_PROJECT_DIR"]}/selftest'}):
            self.assertEqual([], self.build.selftests)

    def test_selftest_unset(self):
        """Test selftests generation when variable is not set."""
        self.assertEqual([], self.build.selftests)

    def test_artifacts_path(self):
        """Test artifacts_path value."""
        self.assertEqual('2021/01/01/5678/redhat:1234/redhat:5678', self.build.artifacts_path)

    @mock_ci_project_dir()
    def test_config_url(self):
        """Test config_url value."""
        rc_content = """
            [state]
            config_file = path/to/file
        """

        with tmp_kcidb_file(self.kcidb_content) as kcidb_file:
            with tmp_rc_file(rc_content):
                build = adapter.Build('redhat:5678', kcidb_file)

        build.upload_file = mock.Mock()
        build.upload_file.return_value = 'http://some/url'

        self.assertEqual('http://some/url', build.config_url)
        build.upload_file.assert_called_with(
            '2021/01/01/5678/redhat:1234/redhat:5678', '.config', source_path='path/to/file'
        )

    @mock_ci_project_dir()
    def test_config_url_empty(self):
        """Test config_url value when there's no config file."""
        with tmp_kcidb_file(self.kcidb_content) as kcidb_file:
            with tmp_rc_file(''):
                build = adapter.Build('redhat:5678', kcidb_file)

        self.assertEqual(None, build.config_url)

    @mock_ci_project_dir()
    def test_output_files(self):
        """Test output_files."""
        rc_content = f"""
        [state]

        tarball_file = {os.environ['CI_PROJECT_DIR']}/tarball
        selftests_file = {os.environ['CI_PROJECT_DIR']}/selftests_file
        selftests_buildlog = {os.environ['CI_PROJECT_DIR']}/selftests_buildlog

        repo_path = something
        kernel_package_url = https://kpackage.url
        """

        pathlib.Path(os.environ['CI_PROJECT_DIR'], 'tarball').touch()
        pathlib.Path(os.environ['CI_PROJECT_DIR'], 'selftests_file').touch()
        pathlib.Path(os.environ['CI_PROJECT_DIR'], 'selftests_buildlog').touch()

        with tmp_kcidb_file(self.kcidb_content) as kcidb_file:
            with tmp_rc_file(rc_content):
                build = adapter.Build('redhat:5678', kcidb_file)

        build.upload_file = mock.Mock()
        build.upload_file.return_value = 'http://some/url'

        self.assertListEqual(
            [{'name': 'tarball', 'url': 'http://some/url'},
             {'name': 'selftests_file', 'url': 'http://some/url'},
             {'name': 'selftests_buildlog', 'url': 'http://some/url'},
             {'name': 'kernel_package_url', 'url': 'https://kpackage.url'}],
            build.output_files
        )

        build.upload_file.assert_has_calls([
            mock.call('2021/01/01/5678/redhat:1234/redhat:5678', 'tarball',
                      source_path=f'{os.environ["CI_PROJECT_DIR"]}/tarball'),
            mock.call('2021/01/01/5678/redhat:1234/redhat:5678', 'selftests_file',
                      source_path=f'{os.environ["CI_PROJECT_DIR"]}/selftests_file'),
            mock.call('2021/01/01/5678/redhat:1234/redhat:5678', 'selftests_buildlog',
                      source_path=f'{os.environ["CI_PROJECT_DIR"]}/selftests_buildlog'),
        ])

    @mock_ci_project_dir()
    def test_output_files_missing(self):
        """Test output_files handles missing files."""
        rc_content = f"""
        [state]

        tarball_file = {os.environ['CI_PROJECT_DIR']}/tarball
        selftests_file = {os.environ['CI_PROJECT_DIR']}/selftests_file
        selftests_buildlog = {os.environ['CI_PROJECT_DIR']}/selftests_buildlog
        """

        with tmp_kcidb_file(self.kcidb_content) as kcidb_file:
            with tmp_rc_file(rc_content):
                build = adapter.Build('redhat:5678', kcidb_file)

        self.assertListEqual([], build.output_files)

    @mock_ci_project_dir()
    def test_output_files_selftests(self):
        """Test output_files adds files to selftests."""
        rc_content = f"""
        [state]

        selftests_buildlog = {os.environ['CI_PROJECT_DIR']}/selftests_buildlog
        """

        pathlib.Path(os.environ['CI_PROJECT_DIR'], 'selftests_buildlog').touch()

        with tmp_kcidb_file(self.kcidb_content) as kcidb_file:
            with tmp_rc_file(rc_content):
                build = adapter.Build('redhat:5678', kcidb_file)

        build.selftests = [
            {'id': 'redhat:5678_0', 'build_id': 'redhat:5678', 'origin': 'redhat'}
        ]

        build.upload_file = mock.Mock()
        build.upload_file.return_value = 'http://some/url'

        self.assertListEqual(
            [{'name': 'selftests_buildlog', 'url': 'http://some/url'}],
            build.output_files
        )

        build.upload_file.assert_has_calls([
            mock.call('2021/01/01/5678/redhat:1234/redhat:5678', 'selftests_buildlog',
                      source_path=f'{os.environ["CI_PROJECT_DIR"]}/selftests_buildlog'),
        ])

        self.assertEqual(
            [
                {
                    'id': 'redhat:5678_0', 'build_id': 'redhat:5678', 'origin': 'redhat',
                    'output_files': [
                       {'name': 'selftests_buildlog', 'url': 'http://some/url'}
                    ]
                }
            ],
            build.selftests,
        )

    @mock_ci_project_dir()
    def test_log_url(self):
        """Test log_url value."""
        rc_content = """
            [state]
            buildlog = path/to/log
        """

        with tmp_kcidb_file(self.kcidb_content) as kcidb_file:
            with tmp_rc_file(rc_content):
                build = adapter.Build('redhat:5678', kcidb_file)

        build.upload_file = mock.Mock()
        build.upload_file.return_value = 'http://some/url'

        self.assertEqual('http://some/url', build.log_url)
        build.upload_file.assert_called_with(
            '2021/01/01/5678/redhat:1234/redhat:5678', 'build.log', source_path='path/to/log'
        )

    @mock_ci_project_dir()
    def test_log_url_empty(self):
        """Test log_url value when there's no log file."""
        with tmp_kcidb_file(self.kcidb_content) as kcidb_file:
            with tmp_rc_file(''):
                build = adapter.Build('redhat:5678', kcidb_file)

        self.assertEqual(None, build.log_url)


@mock.patch.dict(os.environ, ENV_DEFAULT)
class TestBrewCheckout(unittest.TestCase):
    """Test cki.kcidb.adapter.BrewCheckout."""

    @mock_ci_project_dir()
    @mock.patch.dict(os.environ, {'submitter': 'someone@redhat.com'})
    def test_generate(self):
        """Test generate return values."""
        kcidb_content = {
            **KCIDB_DEFAULT,
            'checkouts': [
                {'id': 'redhat:1234', 'origin': 'redhat'}
            ]
        }

        with tmp_kcidb_file(kcidb_content) as kcidb_file:
            checkout = adapter.BrewCheckout('redhat:1234', kcidb_file)

        checkout.generate()

        self.assertEqual(
            {
                'checkouts': [
                    {
                        'id': 'redhat:1234',
                        'origin': 'redhat',
                        'valid': True,
                        'contacts': ['someone@redhat.com'],
                        'tree_name': 'upstream-stable',
                        'misc': adapter.KCIDBAdapter.gitlab_data(),
                    }
                ]
            }, checkout.dump())


@mock.patch.dict(os.environ, ENV_DEFAULT)
class TestBrewBuild(unittest.TestCase):
    """Test cki.kcidb.adapter.BrewBuild."""

    @mock_ci_project_dir()
    @mock.patch.dict(os.environ, {'brew_task_id': '111'})
    def test_generate_brew(self):
        """Test generate return values."""
        kcidb_content = {
            **KCIDB_DEFAULT,
            'checkouts': [
                {'id': 'redhat:1234', 'origin': 'redhat'},
            ],
            'builds': [
                {'id': 'redhat:111_aarch64', 'origin': 'redhat', 'checkout_id': 'redhat:1234',
                 'architecture': 'aarch64'}
            ]
        }

        with tmp_kcidb_file(kcidb_content) as kcidb_file:
            build = adapter.BrewBuild('redhat:111_aarch64', kcidb_file)

        build.brew_task = {
            'Started': '2021-09-10T14:52:21.576302',
            'Finished': '2021-09-10T14:54:21.576302',
        }

        build.generate()

        self.assertEqual(
            {
                'builds': [
                    {
                        'id': 'redhat:111_aarch64',
                        'checkout_id': 'redhat:1234',
                        'origin': 'redhat',
                        'architecture': 'aarch64',
                        'duration': 120,
                        'valid': True,
                        'misc': {
                            'debug': False,
                            **adapter.KCIDBAdapter.gitlab_data(),
                        },
                    }
                ]
            }, build.dump())

    @mock_ci_project_dir()
    @mock.patch.dict(os.environ, {'copr_build': '222'})
    def test_generate_copr(self):
        """Test generate return values."""
        kcidb_content = {
            **KCIDB_DEFAULT,
            'checkouts': [
                {'id': 'redhat:1234', 'origin': 'redhat'},
            ],
            'builds': [
                {'id': 'redhat:111_aarch64', 'origin': 'redhat', 'checkout_id': 'redhat:1234',
                 'architecture': 'aarch64'}
            ]
        }

        with tmp_kcidb_file(kcidb_content) as kcidb_file:
            build = adapter.BrewBuild('redhat:111_aarch64', kcidb_file)

        build.copr_task = {
            'started_on': 1631278724.3241224,
            'ended_on': 1631278744.3241224,
        }

        build.generate()

        self.assertEqual(
            {
                'builds': [
                    {
                        'id': 'redhat:111_aarch64',
                        'checkout_id': 'redhat:1234',
                        'origin': 'redhat',
                        'architecture': 'aarch64',
                        'duration': 20,
                        'valid': True,
                        'misc': {
                            'debug': False,
                            **adapter.KCIDBAdapter.gitlab_data(),
                        },
                    }
                ]
            }, build.dump())

    @mock_ci_project_dir()
    @mock.patch.dict(os.environ, {'brew_task_id': '111'})
    def test_duration_brew(self):
        """Test duration for brew builds."""
        kcidb_content = {
            **KCIDB_DEFAULT,
            'builds': [
                {'id': 'redhat:replace', 'origin': 'redhat', 'checkout_id': 'redhat:1234',
                 'architecture': 'aarch64'}
            ]
        }

        with tmp_kcidb_file(kcidb_content) as kcidb_file:
            build = adapter.BrewBuild('redhat:replace', kcidb_file)

        build.brew_task = {
            'Started': '2021-09-10T14:52:21.576302',
            'Finished': '2021-09-10T14:54:21.576302',
        }

        self.assertEqual(120, build.duration)

    @mock_ci_project_dir()
    @mock.patch.dict(os.environ, {'copr_build': '222'})
    def test_duration_copr(self):
        """Test duration for copr builds."""
        kcidb_content = {
            **KCIDB_DEFAULT,
            'builds': [
                {'id': 'redhat:replace', 'origin': 'redhat', 'checkout_id': 'redhat:1234',
                 'architecture': 'aarch64'}
            ]
        }

        with tmp_kcidb_file(kcidb_content) as kcidb_file:
            build = adapter.BrewBuild('redhat:replace', kcidb_file)

        build.copr_task = {
            'started_on': 1631278724.3241224,
            'ended_on': 1631278744.3241224,
        }

        self.assertEqual(20, build.duration)

    @responses.activate
    @mock_ci_project_dir()
    @mock.patch.dict(os.environ, {'copr_build': '222',
                                  'COPR_SERVER_URL': 'https://copr'})
    def test_copr_task(self):
        """Test copr_task property."""
        kcidb_content = {
            **KCIDB_DEFAULT,
            'builds': [
                {'id': 'redhat:replace', 'origin': 'redhat', 'checkout_id': 'redhat:1234',
                 'architecture': 'aarch64'}
            ]
        }

        with tmp_kcidb_file(kcidb_content) as kcidb_file:
            build = adapter.BrewBuild('redhat:replace', kcidb_file)

        responses.add(responses.GET, 'https://copr/api_2/build_tasks/222/upstream-stable-aarch64',
                      json={'build_task': {'foo': 'bar'}})

        self.assertEqual({'foo': 'bar'}, build.copr_task)

    @responses.activate
    @mock_ci_project_dir()
    @mock.patch.dict(os.environ, {'web_url': 'https://web',
                                  'server_url': 'https://server',
                                  'top_url': 'http://top',
                                  'brew_task_id': '123'})
    @mock.patch('cki.kcidb.adapter.KojiWrap')
    def test_brew_task(self, mock_koji):
        """Test brew_task property."""
        kcidb_content = {
            **KCIDB_DEFAULT,
            'builds': [
                {'id': 'redhat:replace', 'origin': 'redhat', 'checkout_id': 'redhat:1234'}
            ]
        }

        wrap = mock.Mock()
        mock_koji.return_value = wrap
        wrap.taskinfo.return_value = {'foo': 'bar'}

        with tmp_kcidb_file(kcidb_content) as kcidb_file:
            build = adapter.BrewBuild('redhat:replace', kcidb_file)

        build.brew_task  # pylint: disable=pointless-statement

        mock_koji.assert_has_calls([
            mock.call('https://web', 'https://server', 'http://top'),
            mock.call().taskinfo('123')
        ])

        self.assertEqual({'foo': 'bar'}, build.brew_task)


@mock.patch.dict(os.environ, {'KCIDB_DUMPFILE_NAME': 'kcidb/file'})
class TestMain(unittest.TestCase):
    """Test cki.kcidb.adapter.main."""

    @mock.patch('cki.kcidb.adapter.Checkout')
    def test_no_upload(self, mock_checkout):
        """Test main with --no-upload."""
        adapter.main(['checkout', 'redhat:1234', '--no-upload'])

        mock_checkout.assert_has_calls([
            mock.call('redhat:1234', mock.ANY, False)
        ])

    @mock.patch('cki.kcidb.adapter.Checkout')
    def test_checkout(self, mock_checkout):
        """Test main with checkout parameters."""
        adapter.main(['checkout', 'redhat:1234'])

        mock_checkout.assert_has_calls([
            mock.call('redhat:1234', pathlib.Path('kcidb/file'), True),
            mock.call().update(),
            mock.call().update().save(),
        ])

    @mock.patch.dict(os.environ, {'brew_task_id': '111'})
    @mock.patch('cki.kcidb.adapter.BrewCheckout')
    def test_brew_checkout_brew(self, mock_checkout):
        """Test main with checkout parameters."""
        adapter.main(['checkout', 'redhat:1234'])

        mock_checkout.assert_has_calls([
            mock.call('redhat:1234', pathlib.Path('kcidb/file'), True),
            mock.call().update(),
            mock.call().update().save(),
        ])

    @mock.patch.dict(os.environ, {'copr_build': '111'})
    @mock.patch('cki.kcidb.adapter.BrewCheckout')
    def test_brew_checkout_copr(self, mock_checkout):
        """Test main with checkout parameters."""
        adapter.main(['checkout', 'redhat:1234'])

        mock_checkout.assert_has_calls([
            mock.call('redhat:1234', pathlib.Path('kcidb/file'), True),
            mock.call().update(),
            mock.call().update().save(),
        ])

    @mock.patch('cki.kcidb.adapter.Build')
    def test_build(self, mock_build):
        """Test main with build parameters."""
        adapter.main(['build', 'redhat:1234'])

        mock_build.assert_has_calls([
            mock.call('redhat:1234', pathlib.Path('kcidb/file'), True),
            mock.call().update(),
            mock.call().update().save(),
        ])

    @mock.patch.dict(os.environ, {'brew_task_id': '111'})
    @mock.patch('cki.kcidb.adapter.BrewBuild')
    def test_brew_build_brew(self, mock_build):
        """Test main with build parameters."""
        adapter.main(['build', 'redhat:1234'])

        mock_build.assert_has_calls([
            mock.call('redhat:1234', pathlib.Path('kcidb/file'), True),
            mock.call().update(),
            mock.call().update().save(),
        ])

    @mock.patch.dict(os.environ, {'copr_build': '111'})
    @mock.patch('cki.kcidb.adapter.BrewBuild')
    def test_brew_build_copr(self, mock_build):
        """Test main with build parameters."""
        adapter.main(['build', 'redhat:1234'])

        mock_build.assert_has_calls([
            mock.call('redhat:1234', pathlib.Path('kcidb/file'), True),
            mock.call().update(),
            mock.call().update().save(),
        ])
