[metadata]
name = cki-tools
description = "CKI command line tools"
long_description = file: README.md
version = 1
author = Red Hat, Inc.
license = GPLv2+

[options]
packages =
    cki/beaker_tools
    cki/cki_tools
    cki/cki_tools/amqp_bridge
    cki/cki_tools/datawarehouse_report_crawler
    cki/cki_tools/service_metrics
    cki/cki_tools/url_shortener
    cki/cki_tools/webhook_receiver
    cki/deployment_tools
    cki/deployment_tools/grafana
    cki/kcidb
    cki/monitoring_tools
    cki/triager
    pipeline_tools
install_requires =
    anymarkup
    boto3
    cached-property
    cki-lib @ git+https://gitlab.com/cki-project/cki-lib.git/
    datadefinition @ git+https://gitlab.com/cki-project/datadefinition.git/
    datawarehouse-api-lib @ git+https://gitlab.com/cki-project/datawarehouse-api-lib.git/
    kcidb-io @ git+https://github.com/kernelci/kcidb-io.git@v3
    gitpython
    kubernetes
    python-dateutil
    python-gitlab
    pyyaml
    requests
scripts =
    shell-scripts/cki_deployment_acme.sh
    shell-scripts/cki_deployment_clean_docker_images.sh
    shell-scripts/cki_deployment_grafana_backup.sh
    shell-scripts/cki_deployment_grafana_mr.sh
    shell-scripts/cki_deployment_osp_backup.sh
    shell-scripts/cki_deployment_pgsql_backup.sh
    shell-scripts/cki_deployment_pgsql_restore.sh
    shell-scripts/cki_deployment_git_s3_sync.sh
    shell-scripts/cki_tools_git_cache_updater.sh
    shell-scripts/cki_tools_kernel_config_updater.sh

[options.extras_require]
dev =
    freezegun
    kcidb @ git+https://github.com/kernelci/kcidb@883b31681178a1575888a950f56753aa3ba1a962
    mypy
    prometheus_client
    responses
    types-PyYAML
    urllib3_mock
    # If sentry_sdk is already installed, the [flask] extra dependencies are not installed.
    # Workaround by forcing the dependencies to be installed.
    flask>=0.11
    blinker>=1.1
webhook_receiver =
    flask
    gunicorn
    sentry-sdk[flask]
brew =
    koji
kcidb =
    koji
    sentry-sdk
    kcidb @ git+https://github.com/kernelci/kcidb@883b31681178a1575888a950f56753aa3ba1a962
    prometheus_client
deployment_tools =
    jinja2
amqp_bridge =
    cli-proton-python
    prometheus_client
    sentry-sdk
autoscaler =
    openshift-client
    sentry-sdk
deployment_bot =
    prometheus_client
    sentry-sdk
k8s_event_listener =
    prometheus_client
    sentry-sdk
url_shortener =
    flask
    gunicorn
    prometheus_client
    sentry-sdk[flask]
triager =
    prometheus_client
    sentry-sdk
service_metrics =
    crontab
    prometheus_client
    cki-lib[psql] @ git+https://gitlab.com/cki-project/cki-lib.git/
    # If cki-lib is already installed, the [psql] extra dependencies are not installed.
    # Workaround by forcing psycopg2-binary to be installed.
    psycopg2-binary==2.8.6  # https://github.com/psycopg/psycopg2/issues/1311
    sentry-sdk
beaker_broken_machines =
    cki-lib[psql] @ git+https://gitlab.com/cki-project/cki-lib.git/
    # If cki-lib is already installed, the [psql] extra dependencies are not installed.
    # Workaround by forcing psycopg2-binary to be installed.
    psycopg2-binary==2.8.6  # https://github.com/psycopg/psycopg2/issues/1311
    sentry-sdk
datawarehouse_umb_submitter =
    prometheus_client
    sentry-sdk

[options.packages.find]
exclude = tests

[tox:tox]

[testenv]
passenv =
    MINIO_URL
extras =
    amqp_bridge
    autoscaler
    brew
    deployment_bot
    deployment_tools
    dev
    service_metrics
    webhook_receiver
commands = cki_lint.sh cki pipeline_tools

[mypy]
files = cki/cki_tools/yaml.py

[flake8]
exclude=.direnv,.git,.tox*

[pylint.MASTER]
extension-pkg-whitelist=falcon

[SIMILARITIES]
ignore-imports=yes
