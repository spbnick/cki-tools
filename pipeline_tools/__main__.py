"""Deprecated multiplexer main entry point for pipeline_tools."""
import argparse
import os
import sys

from cki_lib import misc
from cki_lib.session import get_session
import gitlab

from pipeline_tools import TOOLS

SESSION = get_session('pipeline_tools')


def main():
    """Run the main CLI entrypoint."""
    parser = argparse.ArgumentParser(description='Pipeline tools')
    parser.add_argument(
        'tool',
        choices=TOOLS.keys(),
        help=('Name of the pipeline tool'),
    )
    parser.add_argument(
        '--project',
        type=str,
        default=os.getenv('GITLAB_PROJECT'),
        help='GitLab project, defaults to GITLAB_PROJECT environment variable',
        required=not os.getenv('GITLAB_PROJECT')
    )
    parser.add_argument(
        '-p', '--pipeline-id',
        type=int,
        help='Gitlab pipeline id, required in retrigger and cancel_pipeline',
    )
    parser.add_argument(
        '-b', '--cki-pipeline-branch',
        type=str,
        help='CKI pipeline branch, required in get_last_successful_pipeline',
    )
    parser.add_argument(
        '-t', '--cki-pipeline-type',
        type=str,
        help='CKI pipeline type, used in get_last_successful_pipeline',
    )
    parser.add_argument(
        "--variables",
        action=misc.StoreNameValuePair,
        nargs='+',
        help=(
            'Variables to replace in the new pipeline triggered '
            'e.g. --variables mail_to=nobody\nUsed in retrigger'
        ),
        default={},
    )
    parser.add_argument(
        '--variable-filter', action=misc.StoreNameValuePair, default={},
        metavar='KEY=VALUE', help='Filter variables with regular expressions' +
        ', used in get_last_successful_pipeline')
    args = parser.parse_args()

    if (args.tool in ('cancel_pipeline', 'retrigger') and args.pipeline_id is None):
        raise Exception("Pipeline_id argument is required in cancel_pipeline")

    if args.tool == 'get_last_successful_pipeline' and \
       args.cki_pipeline_branch is None:
        raise Exception("Cki pipeline branch argument is required in "
                        "get_last_successful_pipeline")

    gitlab_url = misc.get_env_var_or_raise('GITLAB_URL')
    private_token = misc.get_env_var_or_raise('GITLAB_PRIVATE_TOKEN')

    gitlab_instance = gitlab.Gitlab(gitlab_url,
                                    private_token=private_token,
                                    api_version=str(4), session=SESSION)

    project = gitlab_instance.projects.get(args.project)

    func = TOOLS[args.tool].main
    if args.tool == 'retrigger':
        status = func(project, args)
        sys.exit(status)
    elif args.tool == 'cancel_pipeline':
        pipeline = project.pipelines.get(args.pipeline_id)
        func(project, pipeline)
    elif args.tool == 'get_last_successful_pipeline':
        print(func(project, args))
    else:
        raise NotImplementedError


if __name__ == '__main__':
    main()
