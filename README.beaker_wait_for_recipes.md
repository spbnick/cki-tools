# Beaker wait for recipes

`cki.beaker_tools.wait_for_recipes`

Wait until all the recipesets stated in the rc file finished
running.

Due to some Beaker processing delays, results might not be available
even after SKT finishes running. By calling this script we make sure
that when downloading the results in the pipeline all the data contains the
final values.

| Environment variable | Required | Default | Description            |
|----------------------|----------|---------|------------------------|
| `BEAKER_URL`         | Yes      |         | URL to Beaker instance |

| CLI Argument | Required | Default | Description     |
|--------------|----------|---------|-----------------|
| `--rc`       | No       | `rc`    | Path to rc file |
